<%@ page
  	language="java"
  	contentType="text/html;charset=utf-8"
  	pageEncoding="utf-8"
%>

<%@ page
  	import="programacionweb.utilerias.UrlRelativo"
    import="programacionweb.Controladores.*"
%>

<%
  String htmlTitle = "Cifrado y decifrado";
  String mainCss = UrlRelativo.css(request, "/main.css");
  String ico = UrlRelativo.img(request, "/ico.ico");
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><%= htmlTitle %></title>
  <link rel="shortcut icon" href="<%= ico %>">
  <link type = "text/css" rel="stylesheet" href="<%= mainCss %>">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
</head>
<body>
  <section class="section content">
    <form action="?" method="POST">
      <fieldset>
        <legend class="tag" id="legend">Coficicador y decodifcador</legend>
        <div class="column is-half">
          <label class="tag" for="area-de-texto">Mensaje</label>
          <textarea class="input is-rounde" id="area-de-texto" name="area-de-texto" placeholder="Ingresa algo..." ></textarea>
        </div>
        <div>
          <label class="tag" id="error1">${error1}</label>
        </div>
        <div>
          <label class="tag" for="selector">Seleccione cifrado</label>
        </div>
        <div>
          <select class="select" id="selector" name="selector">
            <option value="">Selecciona una codificacion</option>
            <option value="4231">4231</option>
            <option value="13542">13542</option>
            <option value="615243">615243</option>
            <option value="86421357">86421357</option>
            <option value="948372615">948372615</option>
            <option value="54637281">54637281</option>
            <option value="atbash 1">atbash 1</option>
          </select>
        </div>
        <label class="tag" id="error2">${error2}</label>
        <div>
          <button class="button is-success is-rounded" type="submit" id="boton" name="accion" value="boton">Cifrar</button>
          <button class="button is-danger is-rounded" type="submit" id="boton2" name="accion" value="boton2">Decifrar</button>
        </div>
        </fieldset>
    </form>
    <div class="column is-half">
      <label class="tag" for="area-de-texto">Texto cifrado</label>
      <textarea class="input is-rounde" id="resultado" name="resultado">${resultado}</textarea>
    </div>
  </section>
</body>
</html>
