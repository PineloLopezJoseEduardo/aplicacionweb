package programacionweb.controladores;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import programacionweb.modelos.Coder;
import programacionweb.modelos.Decoder;
//@WebServlet("/")
@WebServlet("/cifrado")
public class ControladorInicio extends HttpServlet 
{
  public static String error1,error2,resultado;
  public ControladorInicio()
  {
    super();
  }
  @Override
  protected void doGet(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException 
      {

        String men,enc;
        if(solicitud.getParameter("accion")!=null)
        {
          
          men=solicitud.getParameter("area-de-texto");
          if(men.length()==0)
          {
            error1="Error 0x00 debe dar un texto a cifrar, la longitud del mensaje"+ 
            "\n no puede ser 0";
            solicitud.setAttribute("error1", error1);
            rep(solicitud, respuesta);
          }
          if(men.length()>1000)
          {
            error1="Error 0x01 limite de caracteres es= 1000";
            solicitud.setAttribute("error1", error1);
            rep(solicitud, respuesta);
          }
          enc=solicitud.getParameter("selector");
          if(enc=="")
          {
            error2="Error 0x02 seleccione enciptacion";
            solicitud.setAttribute("error2", error2);
            rep(solicitud, respuesta);
          }

          if(solicitud.getParameter("accion").equalsIgnoreCase("boton"))
          {
            resultado=Coder.condificar(men, enc);
          }
          else
            resultado=Decoder.decodificar(men, enc);
          solicitud.setAttribute("resultado", resultado);
        }
    var contextoServlet = solicitud.getServletContext();
    var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
    despachadorSolicitud.forward(solicitud, respuesta);
  }
  @Override
  protected void doPost(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException{
        String men,enc;
        if(solicitud.getParameter("accion")!=null)
        {
          
          men=solicitud.getParameter("area-de-texto");
          if(men.length()==0)
          {
            error1="Error 0x00 debe dar un texto a cifrar, la longitud del mensaje"+ 
            "\n no puede ser 0";
            solicitud.setAttribute("error1", error1);
            rep(solicitud, respuesta);
          }
          if(men.length()>1000)
          {
            error1="Error 0x01 limite de caracteres es= 1000";
            solicitud.setAttribute("error1", error1);
            rep(solicitud, respuesta);
          }
          enc=solicitud.getParameter("selector");
          if(enc=="")
          {
            error2="Error 0x02 seleccione enciptacion";
            solicitud.setAttribute("error2", error2);
            rep(solicitud, respuesta);
          }
          if(!enc.equalsIgnoreCase("atbash 1"))
          {
            if(solicitud.getParameter("accion").equalsIgnoreCase("boton"))
            {
              resultado=Coder.condificar(men, enc);
            }
            else
              resultado=Decoder.decodificar(men, enc);
            solicitud.setAttribute("resultado", resultado);
          }
          else{
              resultado=Coder.atbash(men);
              solicitud.setAttribute("resultado", resultado);
          }
        }
        rep(solicitud, respuesta);
  }
  protected void rep(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException
      {
        //processRequest(solicitud, respuesta);
        var contextoServlet = solicitud.getServletContext();
        var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
        despachadorSolicitud.forward(solicitud, respuesta);
      }
}
