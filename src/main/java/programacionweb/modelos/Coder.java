package programacionweb.modelos;
public class Coder
{
    public static String condificar(String mensaje, String enc)
    {
        /*
        *En esta linea eliminamos el caracter con valor 10
        *en la tabla de ASCII, debido a que se genera cuando insertamos \n
        *alterando por completamente longitud de caracteres
        */
        mensaje=mensaje.replace((char)10+"", "");
        while(true)
        {
            if(mensaje.length()%enc.length() != 0 )
                mensaje=mensaje+" ";
            else
                break;
        }
        char men[] = mensaje.toCharArray();
        char val[]= enc.toCharArray();
        String cifrado="";
        for(int i=0;i<mensaje.length();i=i+enc.length())
        {
            for(int j=0;j<enc.length();j++)
            {
                int a=Integer.parseInt(val[j]+"");
                cifrado=cifrado+men[a+i-1];
            }
        }
        return cifrado;
    }
    public static String atbash(String men)
    {
        char alf[]={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s',
        't','u','v','w','x','y','z'};

        char nuevo[]={'z','y','x','w','v','u','t','s','r','q','p','o','n','m','l','k','j','i','h',
        'g','f','e','d','c','b','a'};
        
        char msg[]=men.toCharArray();
        String cifrado="";
        boolean enc=true;
        for(int i=0;i<men.length();i++)
        {
            
            for(int j=0;j<nuevo.length;j++)
            {
                if(msg[i]==alf[j])
                {
                    cifrado=cifrado+nuevo[j];
                    enc=true;
                    break;
                }
                else
                    enc=false;
            }
            if(!enc)
            {
                cifrado=cifrado+msg[i];
                System.out.println(msg[i]);
            }
        }
        return cifrado;
    }
}