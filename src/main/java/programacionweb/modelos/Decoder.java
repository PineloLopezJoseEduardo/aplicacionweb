package programacionweb.modelos;
public class Decoder
{
    public static String decodificar(String mensaje, String enc)
    {
        /*
        *En esta linea eliminamos el caracter con valor 10
        *en la tabla de ASCII, debido a que se genera cuando insertamos \n
        *alterando por completamente longitud de caracteres
        */
        mensaje=mensaje.replace((char)10+"", "");
        while(true)
        {
            if(mensaje.length()%enc.length() != 0 )
                mensaje=mensaje+" ";
            else
                break;
        }
        char men[] = mensaje.toCharArray();
        char val[]= enc.toCharArray();
        int inv[]= new int[enc.length()];
        String descifrado="";
        for(int i=0;i<enc.length();i++)
        {
            for(int j=1;j<=enc.length();j++)
            {
                int a= Integer.parseInt(val[j-1]+"");
                if(a==i+1)
                {
                    inv[i]=j;
                }
            }
        }
        for(int i=0;i<mensaje.length();i=i+enc.length())
        {
            for(int j=0;j<enc.length();j++)
            {
                int a=Integer.parseInt(inv[j]+"");
                descifrado=descifrado+men[a+i-1];
            }
        }
        return descifrado;
    }

}